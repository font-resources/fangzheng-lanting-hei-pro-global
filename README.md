# 方正兰亭黑Pro Global字体下载
原始页面：https://www.foundertype.com/index.php/FontInfo/index/id/681 （[存档](https://web.archive.org/web/20210125044636/http://www.foundertype.com/index.php/FontInfo/index/id/681)）

## 下载所有字体
请到[**ttf**](ttf/)目录查看和下载所有字体文件。

### 所有字重（从细到粗）
1. ExtraLight
2. Light
3. Regular
4. DemiBold
5. SemiBold
6. Bold
7. ExtraBold
8. Heavy